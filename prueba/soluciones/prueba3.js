/**
 * Ejercicio 3
 * Algoritmo que permita solicitar 10 números, los cuales serán almacenados en un array, al final, debe visualizar el promedio de esos números
 */

// constantes
const div = document.querySelector("div");
const numeros = [2, 6, 2];

// variables
let promedio = 0;

// entradas
// for (let contador = 0; contador < 10; contador++) {
//     numeros[contador] = Number(prompt("Introduce el numero", 0));
// }



// procesamiento

// sumo los numeros del array con for
// for (let contador = 0; contador < numeros.length; contador++) {
//     promedio += numeros[contador];
// }

// sumo los numeros del array con for of
for (const valor of numeros) {
    promedio += valor;
}

promedio /= numeros.length;

// salidas

div.innerHTML = `El promedio es ${promedio}`
// div.innerHTML = 'El promedio es ' + promedio;
