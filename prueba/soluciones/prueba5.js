/**
Cuando pulse sobre el botón enviar debe comprobar si las direcciones de correo coinciden exactamente, pero sin diferenciar mayúsculas de minúsculas.
Además, debe colocar en el div la palabra "correcto" si está bien o "incorrecto" si no coinciden.
*/

function comprobar() {
    // constantes
    const inputCorreo = document.querySelector("#correo");
    const inputCorreo1 = document.querySelector("#correo1");
    const divSalida = document.querySelector("#caja");

    // variables
    let correo = "";
    let correo1 = "";
    let salida = "";

    // entradas
    correo = inputCorreo.value;
    correo1 = inputCorreo1.value;

    // procesamiento
    if (correo == correo1) {
        salida = "correcto";
    } else {
        salida = "incorrecto";
    }

    // salidas

    divSalida.innerHTML = salida;

}