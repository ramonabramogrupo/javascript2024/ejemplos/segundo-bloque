# Enunciados

## Ejercicio 1

Mientras se carga la página le debéis pedir al usuario 3 números. 
Mostrar los números en la pagina

### Ayudas

Para pedir informacion al usuario:

- funcion prompt

Para mostar los numeros

- utilizar innerHTML
- utilizar document.write


## Ejercicio 2

Mientras se carga la página le debéis pedir al usuario 3 números.

Almacenar esos 3 números en un array.

A continuación, debéis escribir esos 3 números en el cuerpo de la página web dentro de una lista no numerada


# Ejercicio 3

Algoritmo que permita solicitar 10 números, los cuales serán almacenados en un array, al final, debe visualizar el promedio de esos números

# Ejercicio 4

Crear el siguiente formulario y un div en la parte inferior con id caja.

![caja](./div.png)

Cuando pulse sobre el botón enviar me debe colocar el div con el ancho colocado como tamaño y el grosor del borde colocado.


# Ejercicio 5

Crear el siguiente formulario
![cinco](./ej5.png)
 
Cuando pulse sobre el botón enviar debe comprobar si las direcciones de correo coinciden exactamente, pero sin diferenciar mayúsculas de minúsculas.
Además, debe colocar en el div la palabra "correcto" si está bien o "incorrecto" si no coinciden.
