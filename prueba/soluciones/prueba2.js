/**
 * Ejercicio 2
 * Mientras se carga la página le debéis pedir al usuario 3 números.
 * Almacenar esos 3 números en un array.
 * A continuación, debéis escribir esos 3 números en el cuerpo de la página web dentro de una lista no numerada
 */

// constantes

const div = document.querySelector("div");
const numeros = [];
const cantidadNumeros = 3;


// entradas

for (let contador = 0; contador < cantidadNumeros; contador++) {
    numeros[contador] = Number(prompt("Introduce el numero", 0));
}

// procesamiento

// salidas


div.innerHTML = "<ul>";

for (let contador = 0; contador < cantidadNumeros; contador++) {
    div.innerHTML += `<li>${numeros[contador]}</li>`;
    //div.innerHTML += '<li>' + numeros[contador] + '</li>';
}

div.innerHTML += "</ul>";
