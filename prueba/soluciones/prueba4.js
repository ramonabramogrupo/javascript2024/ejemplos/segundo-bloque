/**
 * Cuando pulse sobre el botón enviar me debe colocar el div con el ancho colocado como tamaño y el grosor del borde colocado.
 */
function dibujar() {

    // constantes
    const divCaja = document.querySelector("#caja");
    const inputTamano = document.querySelector("#tamano");
    const inputGrosor = document.querySelector("#grosor");

    // variables
    let tamano = 0;
    let grosor = 0;

    // entradas

    tamano = Number(inputTamano.value);
    grosor = Number(inputGrosor.value);

    // procesamiento

    // salidas

    divCaja.style.width = tamano + "px";
    divCaja.style.border = grosor + "px solid black";


}

