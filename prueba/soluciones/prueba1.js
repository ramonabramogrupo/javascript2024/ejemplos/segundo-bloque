/**
 * Mientras se carga la página le debéis pedir al usuario 3 números. 
 * Mostrar los números en la pagina
 */

// constantes

const div = document.querySelector("div");
const numeros = [];
const cantidadNumeros = 3;


// entradas

// numeros[0] = Number(prompt("Introduce el primer numero", 0));
// numeros[1] = Number(prompt("Introduce el segundo numero", 0));
// numeros[2] = Number(prompt("Introduce el tercer numero", 0));

for (let contador = 0; contador < cantidadNumeros; contador++) {
    numeros[contador] = Number(prompt("Introduce el numero", 0));
}

// procesamiento


// salidas

// div.innerHTML = numeros[0] + "<br>";
// div.innerHTML += numeros[1] + "<br>";
// div.innerHTML += `${numeros[2]}<br>`

for (let contador = 0; contador < cantidadNumeros; contador++) {
    div.innerHTML += `${numeros[contador]}<br>`
    // div.innerHTML += numeros[contador] + "<br>";
}

/*
Puedo realizarlo con una sola variable y un for
*/

// let numero = 0;

// for (let contador = 0; contador < cantidadNumeros; contador++) {
//     numero = Number(prompt("Introduce el numero", 0));
//     div.innerHTML += `${numero}<br>`;
// }
