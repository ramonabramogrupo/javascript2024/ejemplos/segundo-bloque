# Objetivos

Estudio de las instrucciones de control de bucles (lazos);

# Lazos o bucles

Un bucle o lazo son una serie de instrucciones que quiero repetir n veces

Los bucles (loops) en JavaScript son estructuras de control que permiten ejecutar un bloque de código repetidamente mientras se cumpla una condición específica. Los bucles son esenciales en la programación porque permiten automatizar tareas repetitivas y manejar grandes cantidades de datos de manera eficiente. JavaScript soporta varios tipos de bucles

# Instrucciones de control de bucle

Son :
- for : Es el tipo de bucle más común. Se utiliza cuando conoces de antemano el número de iteraciones que necesitas realizar.
```js
for (let i = 0; i < 5; i++) {
    console.log(i); // 0,1,2,3,4
}
```
- while: Se utiliza cuando no se conoce de antemano el número de iteraciones y depende de una condición que se evalúa antes de cada iteración.

```js
let i = 0;
while (i < 5) {
    console.log(i);
    i++;
} // 0,1,2,3,4

```
- do while : Similar al bucle while, pero la condición se evalúa después de ejecutar el bloque de código, lo que garantiza que el bloque de código se ejecute al menos una vez.

```js
let i = 0;
do {
    console.log(i);
    i++;
} while (i < 5); // 0,1,2,3,4
```

- metodos alternativos al foreach : Javascript no dispone de foreach pero tenemos

    - for in : Se utiliza para iterar sobre las propiedades enumerables de un objeto.

```js
const person = {nombre: 'John', edad: 30, ciudad: 'New York'};
for (let key in person) {
    console.log(key + ': ' + person[key]);
} // John,30,New York
```
    - for of : Se utiliza para iterar sobre objetos iterables (como arrays, strings).

```js
const array = [10, 20, 30];
for (let value of array) {
    console.log(value);
}// 10,20,30
```
    - Metodo foreach de un array

# Objetivos de instrucciones de bucle

- Repetir una serie de instrucciones varias veces
- recorrer un array
- recorrer un objeto

# Variables utilizadas en bucles

Tenemos dos variables especiales utilizadas en los bucles:
- contador
- acumulador

## Contadores
Una variable contador en JavaScript es una variable utilizada para llevar un seguimiento del número de veces que ocurre un evento o se ejecuta un bloque de código. Se usa comúnmente en bucles y otras estructuras de control para contar iteraciones, elementos, o cualquier cosa que necesite ser contada.

## Acumuladores
Un acumulador en JavaScript es una variable utilizada para almacenar un valor que se va sumando o acumulando a lo largo del tiempo o de múltiples iteraciones. 

A diferencia de un contador, que generalmente incrementa en uno por cada iteración, un acumulador puede sumar diferentes valores según las necesidades del programa.

```js 
todos=numeros.join("<br>")

```

# Instruccion for

sintaxis


>for(inicializacion;condicion mantenimiento;gestion contadores){
// instrucciones que voy a realizar en cada iteracion
}

## Utilizar for para recorrer array

Tengo un array y quiero recorrer el array con for

```js
const numeros=[12,34,56,78];

for(let contador=0;contador<numeros.length;contador++){
    console.log(numeros[contador]);
    // console.log(numeros[0])
    // console.log(numeros[1])
    // console.log(numeros[2])
    // console.log(numeros[3])
}
```

# while

La sintaxis del while es 

```js
while(condicion de mantenimiento){
    // instrucciones
}
```

Ejemplo de utilizacion del while

>Dejar que el usuario introduzca numeros por teclado hasta que el numero sea negativo

```js
let numero=0;

numero=prompt("introduce un numero");
while(numero>=0){
    console.log(numero);
    numero=prompt("introduce un numero");
}

```

El while es similar al for

```js
let contador=0;
const arrayNumeros=[1,2,45];

for(contador=0;contador<arrayNumeros.length;contador++){
    console.log(arrayNumeros[contador]);
}
```

```js
let contador=0;
const arrayNumeros=[1,2,45];

contador=0;
while(contador<arrayNumeros.length){
    console.log(arrayNumeros[contador]);
    contador++;
}
```

En estos casos es mejor utilizar el for.


# do while

Es igual que el while pero pregunta al final de las instrucciones

La sintaxis del do while es 

```js
do{
    // instrucciones
}while(condicion de mantenimiento);
```

Ejemplo de utilizacion del while

>Dejar que el usuario introduzca numeros por teclado hasta que el numero sea negativo

```js
let numero=0;

do{
    numero=prompt("introduce un numero");
    console.log(numero);
}while(numero>=0);

```


# for of

El bucle for...of en JavaScript se utiliza para iterar sobre objetos iterables, como arrays, strings, maps, sets y otras estructuras de datos que implementan el protocolo iterable. 

Proporciona una sintaxis más simple y legible en comparación con los bucles tradicionales, especialmente cuando solo necesitas los valores del iterable.

## Sintaxis basica

```js
for (const elemento of iterable) {
  // Código a ejecutar por cada elemento
}
```

- const elemento: Una variable que contendrá el valor del elemento actual que se está iterando. Puedes usar let o var en lugar de const, dependiendo de tu caso de uso.

- iterable: El objeto iterable que deseas recorrer.

Podemos utilizar una variable en vez de una constante si tienes intencion de modificar el valor de ella dentro del bucle (no afectara al array)

```js
for (let elemento of iterable) {
  // Código a ejecutar por cada elemento
}
```

## Sintaxis alternativa

Si queremos leer ademas del valor la clave 

```js
for(const [indice,valor] of iterable.entries()){
     // Código a ejecutar por cada elemento
}
```

- indice : tendria el indice del elemento
- valor: tendria el valor del elemento

## Ejemplos de uso

Veamos algunos ejemplos basicos

### Iterar en un array

```js
const numeros=[1,56,7];

for(const elemento of numeros){
    console.log(elemento); 
}

//  cada vuelta
// 1
// 56
// 7

```

Si lo comparamos con un for


```js
let contador=0;
const numeros=[1,56,7];

for(contador=0;contador<numeros.length;contador++){
    console.log(numeros[contador]); 
}

//  cada vuelta
// 1
// 56
// 7

```

La ventaja del for of frente al for es que no entra en las posiciones sin definir

```js
const numeros=[];

numeros[2]=45;
numeros[9]=100;
numeros[0]=1;

for(const elemento of numeros){
    console.log(elemento); 
}

//  cada vuelta
// 1
// 45
// 100

```

Si queremos leer ademas del valor la clave 

```js
const numeros=[];

numeros[2]=45;
numeros[9]=100;
numeros[0]=1;

for(const [indice,valor] of numeros){
    console.log("posicion" + indice); 
    console.log("valor" + valor); 
}

//  cada vuelta
// posicion: 0 , valor: 1
// posicion: 2, valor: 45
// posicion: 9, valor: 100

```

# Objetos iterables

Los elementos  iterables

- arrays 
- strings
