/**
 * Leer los numeros de los dos li
 * 
 * Escribir el numero mayor y menor en los divs mayor y menor
 */

// constantes que apunten al dom

const liNumero1 = document.querySelector("#numero1");
const liNumero2 = document.querySelector("#numero2");
const divMayor = document.querySelector("#mayor");
const divMenor = document.querySelector("#menor");

// variables

let numero1 = 0;
let numero2 = 0;
let mayor = 0;
let menor = 0;

// entradas

numero1 = Number(liNumero1.innerHTML);
numero2 = Number(liNumero2.innerHTML);

// procesamiento
if (numero1 > numero2) {
    mayor = numero1;
    menor = numero2;
} else {
    mayor = numero2;
    menor = numero1;
}

// salidas

divMayor.innerHTML += mayor;
divMenor.innerHTML += menor;





