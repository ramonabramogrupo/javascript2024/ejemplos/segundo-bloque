# Objetivos

Estudiar las instrucciones de control de js

# Operadores 

Cuando utiliceis las instrucciones de control necesitaremos realizar condiciones para las cuales necesitaremos los operadores que ya hemos visto.

Como sobre todo necesitaremos los operadores logicos y de comparacion os vuelvo a colocar la tabla.

# Operadores de Comparación en JavaScript

| Operador | Descripción              | Ejemplo        | Resultado          |
|----------|--------------------------|----------------|--------------------|
| `==`     | Igual a                  | `5 == '5'`     | `true`             |
| `===`    | Estrictamente igual a    | `5 === '5'`    | `false`            |
| `!=`     | No igual a               | `5 != '5'`     | `false`            |
| `!==`    | Estrictamente no igual a | `5 !== '5'`    | `true`             |
| `>`      | Mayor que                | `5 > 3`        | `true`             |
| `<`      | Menor que                | `5 < 3`        | `false`            |
| `>=`     | Mayor o igual que        | `5 >= 5`       | `true`             |
| `<=`     | Menor o igual que        | `5 <= 3`       | `false`            |

# Operadores Lógicos en JavaScript

| Operador | Descripción        | Ejemplo        | Resultado          |
|----------|--------------------|----------------|--------------------|
| `&&`     | Lógico AND         | `true && false`| `false`            |
| ||     | Lógico OR          | `true || false`| `true`             |
| `!`      | Lógico NOT         | `!true`        | `false`            |
| `??`     | Nullish Coalescing | `null ?? 'foo'`| `'foo'`            |
| `?.`     | Optional Chaining  | `obj?.prop`    | `undefined` (si `obj` es `null` o `undefined`) |


# Instrucciones de seleccion

Las instrucciones de control de selección en JavaScript son estructuras que permiten tomar decisiones en el flujo de ejecución del código. 

Estas instrucciones evalúan una o más condiciones y ejecutan diferentes bloques de código según el resultado de esas evaluaciones. 

# Operadores de Control de Selección en JavaScript

| Operador/Declaración | Descripción                           | Ejemplo                                          |
|----------------------|---------------------------------------|--------------------------------------------------|
| `if`                 | Ejecuta un bloque de código si la condición es verdadera | `if (x > 10) { console.log('x es mayor que 10'); }` |
| `else`               | Ejecuta un bloque de código si la condición en `if` es falsa | `if (x > 10) { console.log('x es mayor que 10'); } else { console.log('x es 10 o menor'); }` |
| `else if`            | Especifica una nueva condición si la primera es falsa | `if (x > 10) { console.log('x es mayor que 10'); } else if (x < 5) { console.log('x es menor que 5'); }` |
| `switch`             | Ejecuta uno de varios bloques de código dependiendo del valor de una expresión | `switch (color) { case 'red': console.log('Rojo'); break; case 'blue': console.log('Azul'); break; default: console.log('Otro color'); }` |
| `case`               | Especifica un bloque de código que se ejecutará si el valor de la expresión en `switch` coincide | `case 'red': console.log('Rojo'); break;` |
| `default`            | Especifica un bloque de código que se ejecutará si ninguna de las expresiones `case` coinciden | `default: console.log('Otro color');` |
| `break`              | Termina el bloque de código en `switch` o en bucles      | `break;` |
| `?:` (Operador Ternario) | Asigna un valor basado en una condición                | `let result = (x > 10) ? 'Mayor que 10' : '10 o menor';` |


# Instruccion If

Nos permite seleccionar un camino en funcion de una determinada condicion.

>Sintaxis

```js
if(condicion){
    // instrucciones a realizar si se cumple la condicion
}else{
    // instrucciones a realizar si no se cumple la condicion
}
```

# If anidados

Cuando queremos colocar varios if

```js
if(condicion1){
    // instrucciones a realizar si se cumple la condicion1
    if(condicion2){
        // instrucciones a realizar si se cumplen las condicion1 y condicion2
    }else{
        // instrucciones a realizar si se cumple la condicion1 y si no se cumple la condicion2
    }
}else{
    // instrucciones a realizar si no se cumple la condicion1
    if(condicion3){
        //  instrucciones a realizar si no se cumple la condicion1 y si se cumple la condicion3
    }else{
        //  instrucciones a realizar si no se cumple la condicion1 y si no se cumple la condicion3
    }
}
```

# Seleccion multiple

Para realizar la seleccion multiple podemos utilizar

- if elseif
- switch

## Seleccion multiple con if elseif

Para poder realizar esto

```js
if(condicion1){
    // instrucciones a realizar si se cumple la condicion1
}else if(condicion2){
    // instrucciones a realizar si se cumple la condicion2 y no se cumple la condicion1
}else if(condicion3){
    // instrucciones a realizar si se cumple la condicion2 y no se han cumplido ninguna de las anteriores
}else{
    // instrucciones a realizar si no se han cumplido ninguna de las condiciones
}
```

## Seleccion multiple con switch

Para poder realizar esto

```js
switch(variable){
    case valor1:
        // instrucciones a realizar si variable==valor1
        break;
    case valor2:
        // instrucciones a realizar si variable==valor2
        break;
    default:
        // instrucciones a realizar por defecto
}
```

Sintaxis alternativa

```js
switch(true){
    case (condicion1):
        // instrucciones a realizar si se cumple la condicion1
        break;
    case (condicion2):
        // instrucciones a realizar si se cumple la condicicion2
        break;
    default:
        // instrucciones a realizar por defecto
}
```


# Modificar estilos del DOM

Para poder acceder al DOM utilizamos una constante y el querySelector.

```js
const caja=document.querySelector("#id");
```

Para poder modificar los estilos utilizamos style y la propiedad a modificar

```js
const caja=document.querySelector("#id");

caja.style.propiedad=valor;
```

Las propiedades son las mismas que en CSS pero en camelcase.

- ancho : width 
- alto : height 
- color de fondo : backgroundColor
- color de la letra : color

Los estilos se colocan inline

```js
const caja=document.querySelector("#id");

caja.style.width="300px";
caja.style.backgroundColor="red";
caja.style.color="white";
```


# Introduccion Arrays

En JavaScript, un array (o matriz) es un tipo de dato que permite almacenar múltiples valores en una sola variable. 

Los arrays son útiles para agrupar y gestionar colecciones de datos relacionadas, como listas de nombres, números o cualquier otro tipo de datos.

Esos elementos pueden ser numeros, fechas, textos o elementos del DOM.

## Crear un array

Para crearle se coloca:

const nombreArray = [valor1,valor2, ....]

```js
const nombre=['uno',2,'tres'];
```

Veamos algunos ejemplos 

```js
const arrayVacio = [];
const arrayDeNumeros = [1, 2, 3, 4, 5];
const arrayMixto = [1, 'dos', [4, 5]];
```

## Acceder elemento del array

Para poder acceder a un elemento 

```js
const nombre=['uno',2,'tres'];
 // nombre[indice]
// nombre[0] => 'uno'
// nombre[1] => 2
// nombre[2] => 'tres'
```

Normalmente accederemos a un elemento para:

- leer 

```js
const nombre=['uno',2,'tres'];
alert(nombre[0]); // 'uno'
```
- escribir

```js
const nombre=['uno',2,'tres'];

nombre[1]=23;

// nombre=23 // error porque es una constante
```

# Crear un array de DOM

```js
const nombre = document.querySelectorAll("selector");
nombre[posicion].innerHTML="texto";
```

