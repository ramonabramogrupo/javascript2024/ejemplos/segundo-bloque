/**
 * Leo el numero escrito en el formulario
 * 
 * Si el numero es 1 me debe escribir en el div Salida "Lunes" y colorearlo de azul y letra blanca
 * Si el numero es 2 me debe escribir en el div Salida "Martes" y colorearlo de rojo y letra blanca
 * Si el numero es 3 me debe escribir en el div Salida "Miercoles" y colorearlo de verde y letra blanca
 * Si el numero es 4 me debe escribir en el div Salida "Jueves" y colorearlo de amarillo
 * Si el numero es 5 me debe escribir en el div Salida "Viernes" y colorearlo de naranja y letra blanca
 * Si el numero es 6 me debe escribir en el div Salida "Sabado" y colorearlo de negro y letra blanca
 * Si el numero es 7 me debe escribir en el div Salida "Domingo" y colorearlo de azul y letra blanca
 * 
 */

const icono = document.querySelector("#salida").innerHTML;


function diaSemana() {
    // constantes
    const divSalida = document.querySelector("#salida");
    const inputDia = document.querySelector("#numero");

    // variables
    let dia = 0;
    let nombreDia = "";
    let colorFondo = "";
    let colorLetra = "";

    // entradas
    dia = Number(inputDia.value);

    // procesamiento

    // if (dia == 1) {
    //     nombreDia = "Lunes";
    //     colorFondo = "blue";
    //     colorLetra = "white";
    // } else if (dia == 2) {
    //     nombreDia = "Martes";
    //     colorFondo = "red";
    //     colorLetra = "white";
    // } else if (dia == 3) {
    //     nombreDia = "Miercoles";
    //     colorFondo = "green";
    //     colorLetra = "white";
    // } else if (dia == 4) {
    //     nombreDia = "Jueves";
    //     colorFondo = "yellow";
    //     colorLetra = "white";
    // } else if (dia == 5) {
    //     nombreDia = "Viernes";
    //     colorFondo = "orange";
    //     colorLetra = "white";
    // } else if (dia == 6) {
    //     nombreDia = "Sabado";
    //     colorFondo = "black";
    //     colorLetra = "white";   
    // } else if (dia == 7) {
    //     nombreDia = "Domingo";
    //     colorFondo = "blue";
    //     colorLetra = "white";
    // } else {
    //     nombreDia = "desconocido";
    //     colorFondo = "red";
    //     colorLetra = "white";
    // }

    switch (dia) {
        case 1:
            nombreDia = "Lunes";
            colorFondo = "blue";
            colorLetra = "white";
            break;
        case 2:
            nombreDia = "Martes";
            colorFondo = "red";
            colorLetra = "white";
            break;
        case 3:
            nombreDia = "Miercoles";
            colorFondo = "green";
            colorLetra = "white";
            break;
        case 4:
            nombreDia = "Jueves";
            colorFondo = "yellow";
            colorLetra = "white";
            break;
        case 5:
            nombreDia = "Viernes";
            colorFondo = "orange";
            colorLetra = "white";
            break;
        case 6:
            nombreDia = "Sabado";
            colorFondo = "black";
            colorLetra = "white";
            break;
        case 7:
            nombreDia = "Domingo";
            colorFondo = "blue";
            colorLetra = "white";
            break;
        default:
            nombreDia = "desconocido";
            colorFondo = "red";
            colorLetra = "white";
            break;
    }

    // salidas
    divSalida.innerHTML = icono + nombreDia;
    divSalida.style.backgroundColor = colorFondo;
    divSalida.style.color = colorLetra;

}