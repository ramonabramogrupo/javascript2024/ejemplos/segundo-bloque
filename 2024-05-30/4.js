/**
 * Leo la nota escrita en el formulario 
 * 
 * Y en el div de salida me debe colocar la nota en texto atendiendo a el siguiente baremo
 * 
 * [0-5) => Insuficiente
 * [5-6) => Suficiente
 * [6-7) => Bien
 * [7-9) => Notable
 * [9-10] => Sobresaliente
 * 
 */

const icono = document.querySelector("#salida").innerHTML;

function calcularNota() {

    // constantes
    const inputNota = document.querySelector("#nota");
    const divSalida = document.querySelector("#salida");

    // variables
    let nota = 0;
    let notaTexto = "";

    // entradas
    nota = Number(inputNota.value);

    // procesamiento
    switch (true) {
        case (nota < 5):
            notaTexto = "Insuficiente";
            break;
        case (nota < 6):
            notaTexto = "Suficiente";
            break;
        case (nota < 7):
            notaTexto = "Bien";
            break;
        case (nota < 9):
            notaTexto = "Notable";
            break;
        case (nota <= 10):
            notaTexto = "Sobresaliente";
            break;
        default:
            notaTexto = "Error";

    }

    // salidas
    //divSalida.innerHTML = icono + "&nbsp;$nbsp;" +notaTexto;
    divSalida.innerHTML = `${icono} &nbsp; &nbsp; ${notaTexto}`;

}

