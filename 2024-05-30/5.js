/**
 * 
 */

// variables y constantes
const div = document.querySelector("div");

// crear un array con 20 numeros enteros
const arrayNumeros = [
    23, 45, 67, 89, 101, 123, 145, 167, 189, 211, 233, 255, 277, 299, 321, 343, 365, 387, 409, 431
];


// leer el elemento 1 del array (2 puesto)

div.innerHTML += `El elemento 1 del array es: ${arrayNumeros[1]}`;

// leer el elemento 10 del array (11)

div.innerHTML += `El elemento 10 del array es: ${arrayNumeros[10]}`;

// escribir en el primer elemento del array

arrayNumeros[0] = 'primero';

// imprimir el array entero

div.innerHTML += `<br>${arrayNumeros}`;