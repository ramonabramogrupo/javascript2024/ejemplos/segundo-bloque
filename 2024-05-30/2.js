/**
 * Debe calcular el numero mayor de los 3 y almacenarlo en mayor
 * 
 * Quiero que escriba los 3 numeros y el numero mayor en el html
 */

// variables

let numero1 = 2000;
let numero2 = 230;
let numero3 = 100;
let mayor = 0;

// constantes

const liNumero1 = document.querySelector("#numero1");
const liNumero2 = document.querySelector("#numero2");
const liNumero3 = document.querySelector("#numero3");
const divMayor = document.querySelector("#mayor");

//entradas


//procesamiento

// comprobar cual de los 3 numeros es mayor y almacenarlo en la variable mayor
// if (numero1 > numero2) {
//     if (numero1 > numero3) {
//         mayor = numero1;
//     } else {
//         mayor = numero3;
//     }
// } else {
//     if (numero2 > numero3) {
//         mayor = numero2;
//     } else {
//         mayor = numero3;
//     }
// }


if (numero1 > numero2 && numero1 > numero3) {
    mayor = numero1;
} else if (numero2 > numero1 && numero2 > numero3) {
    mayor = numero2;
} else {
    mayor = numero3;
}


//salidas

liNumero1.innerHTML = numero1;
liNumero2.innerHTML = numero2;
liNumero3.innerHTML = numero3;
divMayor.innerHTML = mayor;
