// variables y constantes

// constantes que apunten a los elementos del DOM
const divOrigen =document.querySelector("#origen");
const divCopia1 = document.querySelector("#copia1");
const divCopia2 = document.querySelector("#copia2");

// variable texto del primer div
let texto="";

// entradas
texto=divOrigen.innerHTML;

// procesamiento

// salidas
divCopia1.innerHTML=texto;
divCopia2.innerHTML=texto;