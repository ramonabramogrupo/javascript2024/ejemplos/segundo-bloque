/**
 * Le pido al usuario que me escriba el lado de un cuadrado
 * 
 * Calculo el area y el perimetro de ese cuadrado
 * 
 * Escribimos en los li el lado, el area y el perimetro
 */

// constantes que apunten al dom

const liLado=document.querySelector("#lado");
const liArea=document.querySelector("#area");
const liPerimetro=document.querySelector("#perimetro");

// variables
let lado=0;
let area=0;
let perimetro=0;

// entradas
lado=prompt("Introduce el lado",0);

// procesamiento
area=lado**2;
perimetro=4*lado;

// salidas

liLado.innerHTML+=lado;

liArea.innerHTML+=area;

liPerimetro.innerHTML+=perimetro;
