// variables y constantes

// variables para almacenar informacion temporalmente
let texto1='';
let texto2='';

// constante que apunten a elementos del DOM
const divOrigen = document.querySelector('#divOrigen');
const divDestino = document.querySelector('#divDestino');

// entradas

texto1=divOrigen.innerHTML;
texto2=divDestino.innerHTML;

// procesamiento

// salidas
divDestino.innerHTML=texto1;
divOrigen.innerHTML=texto2;