/**
 * Pido por teclado el radio de un circulo
 * prompt("Introduce radio",0)
 * 
 * Calcular el area y el perimetro de un circulo del radio dado
 * 
 * Debe escribir en los li el radio, el area y el perimetro
 */

// crear constantes que apunten a los objetos del dom a utilizar
const liRadio = document.querySelector("#radio");
const liArea = document.querySelector("#area");
const liPerimetro = document.querySelector("#perimetro");

// crear variable
let area = 0;
let perimetro = 0;
let radio = 0;

// entradas
radio = prompt("Introduce radio", 0);

// procesar
area = Math.PI * radio ** 2;
perimetro = 2 * Math.PI * radio;

// salida
liRadio.innerHTML+=radio;
liArea.innerHTML+=area;
liPerimetro.innerHTML+=perimetro;
