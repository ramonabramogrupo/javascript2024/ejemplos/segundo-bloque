function sumar() {
    const input1 = document.querySelector("#numero1");
    const input2 = document.querySelector("#numero2");

    let numero1 = 0;
    let numero2 = 0;
    let sumar = 0;

    numero1 = Number(input1.value);
    numero2 = Number(input2.value);

    sumar = numero1 + numero2;

    mostrar(sumar);


}

function restar() {
    const input1 = document.querySelector("#numero1");
    const input2 = document.querySelector("#numero2");

    let numero1 = 0;
    let numero2 = 0;
    let restar = 0;

    numero1 = Number(input1.value);
    numero2 = Number(input2.value);

    restar = numero1 - numero2;

    mostrar(restar);

}

function mostrar(mensaje) {
    const divSalida = document.querySelector("#salida");

    divSalida.innerHTML = mensaje;
}