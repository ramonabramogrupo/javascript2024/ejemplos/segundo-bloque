/**
 * Escribo en el input el radio de un circulo
 * Cuando pulso sobre el boton 
 * Area ==> Calcular el area
 * Perimetro ==> Calcular el perimetro
 * 
 * Mostrar en el div Salida el resultado calculado
 * 
 * Dibujar en el div circulo un circulo con el radio dado
 */

// funciones

/**
 * Funcion que calcula el area de un circulo
 */
function area() {
    // variables y constantes
    const radioInput = document.querySelector("#radio");
    let area = 0;
    let radio = 0;

    // entradas
    radio = Number(radioInput.value);

    // procesamiento
    area = Math.PI * radio ** 2;

    // salidas
    mostrar(area, radio);

}

/**
 * Funcion que calcula el perimetro de un circulo
 */
function perimetro() {
    // variables y constantes
    const radioInput = document.querySelector("#radio");
    let perimetro = 0;
    let radio = 0;


    // entradas
    radio = Number(radioInput.value);

    // procesamiento
    perimetro = 2 * Math.PI * radio;

    // salidas
    mostrar(perimetro, radio);

}

function mostrar(mensaje, radio) {
    const divSalida = document.querySelector("#salida");
    const divCirculo = document.querySelector("#circulo");

    // escribo el dato a mostrar
    divSalida.innerHTML = mensaje;

    // dibujo el circulo
    divCirculo.style.width = (radio * 10) + "px";
    divCirculo.style.height = (radio * 10) + "px";
}

