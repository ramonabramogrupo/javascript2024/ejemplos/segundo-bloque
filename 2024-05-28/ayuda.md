a[[_TOC_]]

# Objetivo

Explicar el funcionamiento del standard markdown

# Encabezados

Para crear encabezados utilizo #

- h1: #
- h2: ##
- h3: ###

# Listas

Para poder realizar listas 
- numeradas
- no numeradas

## Numeradas

Para realizar una lista numerada

1- primer elemento

2- segundo elemento.

```html
<ol>
    <li> primer elemento </li>
    <li> segundo elemento </li>
</ol>
```

## No numeradas

Para realizar un lista no numerada

- santander
- loredo

```html
<ul>
<li>Santander</li>
<li>Loredo</li>
</ul>
```

# Citas

Para destacar un parrafo como cita (> texto)

> parrafo destacado

# Enlace

Para poder realizar enlaces.

    [texto a mostrar](enlace)

[Pulsa para mas informacion](https://www.alpeformacion.es)


```html
<a href="https://www.alpeformacion.es> Pulsa para mas informacion </a>
```

