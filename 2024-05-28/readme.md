[[_TOC_]]

# Objetivos

JavaScript es un lenguaje de programación de alto nivel y dinámico que se utiliza principalmente en el desarrollo web para crear páginas interactivas. Se ejecuta en el navegador web, lo que permite manipular HTML y CSS en tiempo real. A continuación, te proporciono una introducción básica a la estructura de un programa en JavaScript, cómo introducir datos, cómo mostrar salidas y algunos ejemplos.

En estas primeros ejemplos vamos a entender el funcionamiento general de javascript (js)

# JS, HTML, CSS

Diferenciar JavaScript de otros lenguajes de programación utilizados en el desarrollo web, como HTML y CSS.

# Sintaxis 

Escribir y ejecutar el programa "Hola, mundo!".

De esta forma entenderemos la sintaxis basica de las instrucciones de js

Ademas veremos como podemos incluir JS en el codigo HTML

- directamente

```html
<script>
document.write("hola mundo");
</script>
```
- en otro archivo

```html
<script src="fichero.js"></script>
```
> En el otro archivo fichero.js

```js
document.write("hola mundo");
```

# Entorno de desarrollo

Configurar un entorno de desarrollo básico para JavaScript, utilizando un editor de texto o un entorno de desarrollo integrado (IDE) como Visual Studio Code.

Aprender a utilizar la consola del navegador para ejecutar y depurar código JavaScript.

# Variables

El objetivo de una variable en JavaScript (y en cualquier lenguaje de programación) es almacenar datos que pueden ser utilizados y manipulados a lo largo de la ejecución de un programa. 

Las variables permiten a los desarrolladores guardar valores, modificar esos valores, y referenciarlos mediante un nombre en lugar de tener que utilizar los valores directamente.

Para declarar variables :

- var: Alcance global o de función.
- let: Alcance de bloque.

```js
var edad = 30; // Variable global
let nombre = "Juan"; // Variable de bloque
```

Usar variables facilita la reusabilidad del código y mejora la mantenibilidad. En lugar de repetir el mismo valor en múltiples lugares, se puede usar una variable.

Las variables proporcionan una capa de abstracción, haciendo el código más legible y entendible. En lugar de usar valores crudos, se usan nombres descriptivos.

```js
let precioProducto = 20;
let cantidadComprada = 3;
let total = precioProducto * cantidadComprada;
```

## Ejemplo

Veamos un ejemplo como los que hemos realizado en clase

```js
// Declarar variables para almacenar datos del usuario
let nombre = "Ana";
let edad = 28;
let ciudad = "Madrid";

// Usar las variables para crear un mensaje
let mensaje = "Hola, mi nombre es " + nombre + ". Tengo " + edad + " años y vivo en " + ciudad + ".";

// Mostrar el mensaje en la consola
console.log(mensaje);

// Modificar el valor de una variable
edad = 29;

// Crear un nuevo mensaje con el valor actualizado
mensaje = "Ahora tengo " + edad + " años.";

// Mostrar el nuevo mensaje en la consola
console.log(mensaje);
```

En este ejemplo:

- Se declaran y se asignan valores a las variables nombre, edad, y ciudad.
- Se utiliza una variable (mensaje) para construir una cadena que combina texto estático con los valores de las variables.
- Se muestra el mensaje en la consola.
- Se modifica el valor de edad y se crea un nuevo mensaje con el valor actualizado.

>Este enfoque hace que el código sea más fácil de entender y mantener, ya que los valores utilizados pueden cambiarse en un solo lugar (la declaración de la variable) en lugar de en múltiples ubicaciones a lo largo del código.

# Constantes

En JavaScript, una constante es un tipo de variable cuyo valor no puede cambiar una vez asignado. 

Se utiliza para almacenar valores que deben permanecer constantes a lo largo de la ejecución del programa. 

## Declarar

Las constantes se declaran utilizando la palabra clave const.

La sintaxis de la declaracion de una constante

```js
const nombreConstante = valor;
```

## Características de las Constantes

### Asignación Única
Una vez que una constante ha sido declarada y asignada, su valor no puede ser modificado.

### Ámbito de Bloque
Al igual que las variables declaradas con let, las constantes tienen un ámbito de bloque. Esto significa que solo son accesibles dentro del bloque donde fueron definidas.

### Inmutabilidad
El valor de una constante no puede ser re-asignado, pero si la constante es un objeto o un array, sus propiedades o elementos pueden ser modificados.

Esta es una caracteristica muy importante que vamos a aprovechar para apuntar a los objetos del DOM.

```html
<!DOCTYPE html>
<html>
<head>
    <title>Manipulación del DOM</title>
</head>
<body>
    <div id="mensaje">Ejemplo</div>
    <script>
        // creo una constante que apunta al div
        const divMensaje=document.querySelector("#mensaje");

        // leo el texto del div
        alert(divMensaje.innerHTML)

        // modifico el contenido del div
        divMensaje.innerHTML="Ejemplo de clase";
    </script>
</body>
</html>

```

# Operadores

Cuando tengamos que realizar expresiones en JS tendremos que utilizar operadores.


En JavaScript, hay una variedad de operadores que se utilizan para realizar diferentes tipos de operaciones en datos. 


| Tipo de Operador          | Operador       | Descripción                                               | Ejemplo                                                      |
|---------------------------|----------------|-----------------------------------------------------------|--------------------------------------------------------------|
| **Aritméticos**           | `+`            | Suma                                                      | `a + b`                                                      |
|                           | `-`            | Resta                                                     | `a - b`                                                      |
|                           | `*`            | Multiplicación                                            | `a * b`                                                      |
|                           | `/`            | División                                                  | `a / b`                                                      |
|                           | `%`            | Módulo (resto)                                            | `a % b`                                                      |
|                           | `**`           | Exponenciación                                            | `a ** b`                                                     |
| **Asignación**            | `=`            | Asignación                                                | `a = b`                                                      |
|                           | `+=`           | Asignación de suma                                        | `a += b`                                                     |
|                           | `-=`           | Asignación de resta                                       | `a -= b`                                                     |
|                           | `*=`           | Asignación de multiplicación                              | `a *= b`                                                     |
|                           | `/=`           | Asignación de división                                    | `a /= b`                                                     |
|                           | `%=`           | Asignación de módulo                                      | `a %= b`                                                     |
|                           | `**=`          | Asignación de exponenciación                              | `a **= b`                                                    |
| **Comparación**           | `==`           | Igualdad                                                  | `a == b`                                                     |
|                           | `===`          | Igualdad estricta                                         | `a === b`                                                    |
|                           | `!=`           | Desigualdad                                               | `a != b`                                                     |
|                           | `!==`          | Desigualdad estricta                                      | `a !== b`                                                    |
|                           | `>`            | Mayor que                                                 | `a > b`                                                      |
|                           | `>=`           | Mayor o igual que                                         | `a >= b`                                                     |
|                           | `<`            | Menor que                                                 | `a < b`                                                      |
|                           | `<=`           | Menor o igual que                                         | `a <= b`                                                     |
| **Lógicos**               | `&&`           | AND lógico                                                | `a && b`                                                     |
|                           | `||`           | OR lógico                                                 | `a || b`                                                     |
|                           | `!`            | NOT lógico                                                | `!a`                                                         |
| **Incremento/Decremento** | `++`           | Incremento                                                | `a++` o `++a`                                                |
|                           | `--`           | Decremento                                                | `a--` o `--a`                                                |
| **Concatenación**         | `+`            | Concatenación de cadenas                                  | `"Hola, " + "mundo!"`                                        |
| **Condicional**           | `? :`          | Operador ternario (condicional)                           | `condicion ? expr1 : expr2`                                  |
| **Desestructuración**     | `[ ]`          | Desestructuración de arrays                                | `let [a, b] = [1, 2]`                                        |
|                           | `{ }`          | Desestructuración de objetos                              | `let {nombre, edad} = {nombre: "Juan", edad: 30}`            |
| **Bit a Bit**             | `&`            | AND bit a bit                                             | `a & b`                                                      |
|                           | `|`            | OR bit a bit                                              | `a | b`                                                      |
|                           | `^`            | XOR bit a bit                                             | `a ^ b`                                                      |
|                           | `~`            | NOT bit a bit                                             | `~a`                                                         |
|                           | `<<`           | Desplazamiento a la izquierda                             | `a << b`                                                     |
|                           | `>>`           | Desplazamiento a la derecha                               | `a >> b`                                                     |
|                           | `>>>`          | Desplazamiento a la derecha sin signo                     | `a >>> b`                                                    |


## Operador ${}

En JavaScript, el uso del operador ${} dentro de una cadena se llama Interpolación de Cadenas o Interpolación de Plantillas (Template Literals). 

Esta característica permite incluir expresiones dentro de las cadenas, proporcionando una forma más sencilla y legible de construir cadenas de texto.

```js
let nombre = "Juan";
let edad = 25;
let mensaje = `Hola, mi nombre es ${nombre} y tengo ${edad} años.`;

console.log(mensaje);
// Salida: Hola, mi nombre es Juan y tengo 25 años.

```


# Estructura basica de un programa

La estructura básica de un programa de software puede dividirse en cuatro partes fundamentales: declaracion, entrada, procesamiento y salida. 

A continuación, se muestra cómo se puede implementar esta estructura en JavaScript.

```html
<!DOCTYPE html>
<html>
<head>
    <title>Programa Básico en JavaScript</title>
</head>
<body>
    <div id="salida"></div>
    <script>
        // declaracion de variables y constantes
        let nombre="";
        let edad=0;
        let mensaje="";

        // Entrada de datos
        nombre = prompt("¿Cuál es tu nombre?");
        edad = prompt("¿Cuál es tu edad?");

        // Procesamiento de datos
        mensaje = "Hola, " + nombre + ". Tienes " + edad + " años.";

        // Salida de datos
        alert(mensaje);
        console.log(mensaje);
        document.querySelector("#salida").innerHTML = mensaje;
    </script>
</body>
</html>

```

# Declaracion

En esta seccion declararemos las variables y las contantes como ya hemos visto.

# Introducir datos

Para poder introducir datos que almacenaremos en las variables podemos realizarlo.

- prompt
- confirm
- formularios
- DOM


## prompt

Solicita una entrada del usuario mediante un cuadro de diálogo. 

>Se utiliza principalmente para obtener datos del usuario de manera sencilla.

La sintaxis de prompt es 

```js
variable=prompt("mensaje","valor por defecto");
```

Veamos un ejemplo:

```js
let numero=prompt("Introduce un numero",0);
```

# confirm

Muestra un cuadro de diálogo con un mensaje y dos botones: Aceptar y Cancelar. 

La sintaxis de confirm es 

```js
variable=confirm("mensaje");
```

Retorna true si se pulsa Aceptar y false si se pulsa Cancelar.

Veamos un ejemplo:

```js
let respuesta = confirm("¿Estás seguro de que deseas continuar?");

// la instruccion if nos permite realizar una seleccion simple
if (respuesta) {
    console.log("Continuando...");
} else {
    console.log("Cancelado.");
}
```

## DOM

Podemos leer contenido desde la propia web.

```html
<!DOCTYPE html>
<html>
<head>
    <title>Manipulación del DOM</title>
</head>
<body>
    <div id="mensaje">Ejemplo de clase</div>
    <script>
        let nombre = "Juan";
        document.querySelector()
    </script>
</body>
</html>

```

## Formularios

Los formularios son un metodo muy comun de introducir datos 

Para poder acceder a los elementos del formulario incialmente utilizaremos la propiedad value

```html
<!DOCTYPE html>
<html>
<head>
    <title>Formulario de Datos</title>
</head>
<body>
    <h2>Formulario de Datos</h2>
    <form id="datosForm">
        <label for="nombre">Nombre:</label>
        <input type="text" id="nombre" name="nombre"><br><br>
        
        <label for="edad">Edad:</label>
        <input type="number" id="edad" name="edad"><br><br>
        
        <label for="fechaNacimiento">Fecha de Nacimiento:</label>
        <input type="date" id="fechaNacimiento" name="fechaNacimiento"><br><br>
        
        <button type="button" onclick="procesarDatos()">Enviar</button>
    </form>
    
    <div id="resultado"></div>

    <script>
        function procesarDatos() {
            // Constantes para apuntar a los elementos del formulario
            const nombreInput = document.querySelector('#nombre');
            const edadInput = document.querySelector('#edad');
            const fechaNacimientoInput = document.querySelector('#fechaNacimiento');
            const resultadoDiv = document.querySelector('#resultado');

            // Variables para almacenar los valores de los campos
            let nombre = nombreInput.value;
            let edad = edadInput.value;
            let fechaNacimiento = fechaNacimientoInput.value;

            // Procesar los datos
            let mensaje = `Nombre: ${nombre}, Edad: ${edad}, Fecha de Nacimiento: ${fechaNacimiento}`;

            // Mostrar los datos en el elemento 'resultado'
            resultadoDiv.innerText = mensaje;
        }
    </script>
</body>
</html>
```

# Salida de datos

Para mostrar datos en JavaScript
- console.log()
- alert() 
- document.write
- formularios
- se manipula el DOM para mostrar contenido en la página web.

## Consola

Muestra mensajes en la consola del navegador


```js
console.log("Hola, mundo!");
console.log("Nombre: " + nombre);
```

## Alert

Muestra un cuadro de alerta con un mensaje al usuario. 

>Es útil para notificaciones simples, pero no es recomendable para aplicaciones complejas.

```js
alert("Hola clase");
```

## document.write

Escribe directamente en el documento HTML. 

```js
// el operador '+' es para concatenar
document.write("<p>Hola, " + nombre + "!</p>");
```

>No se recomienda su uso en aplicaciones modernas porque puede sobrescribir el contenido de la página. Es decir si lo utilizamos cuando la pagina esta cargada esta se borra y solo tendria el contenido nuevo.


## DOM

Accedemos al elemento donde queremos escribir y despues colocamos el mensaje a mostrar

Inserta o modifica el contenido de elementos HTML. Esta es la técnica más utilizada para actualizar dinámicamente la interfaz de usuario.

```js
document.querySelector("selector").innerHTML = "mensaje";
```