/**
 * En el formulario quiero colocar las caracteristicas
 * que se van a aplicar al div salida
 *
 * Cuando pulso el boton del formulario debe aplicar esas caracteristicas
 */
function dibujar() {
    // constantes
    const divSalida = document.querySelector("#salida");


    // variables
    let color = "";
    let anchura = 0;
    let altura = 0;
    let fondo = "";

    // entradas

    color = function dibujar() {
        // constantes
        const divSalida = document.querySelector("#salida");
        const inputColor = document.querySelector("#color");
        const inputAnchura = document.querySelector("#anchura");
        const inputAltura = document.querySelector("#altura");
        const inputFondo = document.querySelector("#fondo");

        // variables
        let color = "";
        let anchura = 0;
        let altura = 0;
        let fondo = "";

        // entradas

        color = inputColor.value;
        anchura = inputAnchura.value + "px";
        altura = inputAltura.value + "px";
        fondo = inputFondo.value;


        // procesamiento

        // salidas

        divSalida.style.backgroundColor = fondo;
        divSalida.style.width = anchura;
        divSalida.style.height = altura;
        divSalida.style.color = color;
    }
    anchura = inputAnchura.value + "px";
    altura = inputAltura.value + "px";
    fondo = inputFondo.value;


    // procesamiento

    // salidas

    divSalida.style.backgroundColor = fondo;
    divSalida.style.width = anchura;
    divSalida.style.height = altura;
    divSalida.style.color = color;
}

// function dibujar() {
//     document.querySelector("#salida").style.color = document.querySelector("#color").value;
//     document.querySelector("#salida").style.width = document.querySelector("#anchura").value + "px";
//     document.querySelector("#salida").style.height = document.querySelector("#altura").value + "px";
//     document.querySelector("#salida").style.backgroundColor = document.querySelector("#fondo").value;

// }