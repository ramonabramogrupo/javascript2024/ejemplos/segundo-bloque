/**
 * Pedir un numero por teclado
 * 
 * utilizar ese numero como ancho y alto del div salida
 */

// constantes
const divSalida = document.querySelector("#salida");

// variables
let numero = 0;

// entradas
numero = prompt("Introduce un numero", 0);

// procesamiento

// salidas

divSalida.style.width = numero + "px";
divSalida.style.height = numero + "px";




